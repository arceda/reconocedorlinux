#include "utils.h"
#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <vector>

//#include <cv.h>
//#include <highgui.h>

using namespace std;





//
// TEST (FACE TO PERSONS SIMILARITIES) V 1.0
// Encargado de realizar el reconocimiento de un rostro, retorna una lista de personas similares.
// Parametros:
// SRC-model              -> Archivo conteniendo el modelo entrenado, este es generado con train.cpp
// path_photo			  -> Ruta del archivo o photo a reconocer
// SCI-threshold          -> Margen de reconocimiento,se recomienda valores de 0.5f
// n-persons              -> Numero de personas similares 
// result                 -> Ruta del archivo donde se escribiran los resultados de las personas similares.        
//
//parametros C:\Servidor\Apache\htdocs\videoFirma\faces\model.txt C:\Servidor\Apache\htdocs\videoFirma\faces\vicente@gmail.com\1_face.jpg 0.2f 5 C:\Servidor\Apache\htdocs\videoFirma\faces\results.txt



// print usage information
/*
inline void Usage(const char *exe) {
	cerr << "Usage: " << endl
	<< "  " << exe << "SRC-model path_photo SCI-threshold n-persons result" << endl;
}*/

int main(int ac, char **av) {
	
	//cout << "TESTING..." << endl << endl;

	//cout << "TESTING..." << endl << endl;

	const string src_model_file		= av[1];
	const string path_photo			= av[2];
	const double sci_t				= atof(av[3]);
	const int n_persons				= atoi(av[4]);
	const string file_result		= av[5];

	//const string src_model_file		= "my_model.txt";
	//const string path_photo			= "face.jpg";
	//const double sci_t			= 0.2f;
	//const int n_persons			= 5;
	//const string file_result		= "result.txt";

	//cout << "src_model_file "	<< src_model_file << endl;
	//cout << "path_photo "		<< path_photo << endl;
	//cout << "sci "				<< sci_t << endl;
	//cout << "n_persons "		<< n_persons << endl;
	//cout << "file_result "		<< file_result << endl;


	//vector<string> test_sample_list;
	//LoadSampleList(test_sample_list_file, &test_sample_list);

	//cout<<"openmodel..."<<endl;

	SRCModel *src_model = LoadSRCModel(src_model_file);

	//cout<<"finish openmodel..."<<endl;

	
	//ofstream destino("D:\\yaleBD\\croppedYaleTestingResult.txt");
	ofstream destino(file_result.c_str());

	CvMat *y = LoadSample(path_photo, src_model->sample_size_);
	
	//string name = Recognize(src_model, y, sci_t, (path_photo + ".x").c_str(), (path_photo + ".r").c_str());	

	vector<string> result;
	double sci = RecognizeSimilarity(src_model, y, sci_t, (path_photo + ".x").c_str(), (path_photo + ".r").c_str(), &result, n_persons);

	destino << sci << endl;
	for (int i = 0; i < result.size(); i++){
		destino << (result)[i] << endl;
	}
		
	cvReleaseMat(&y);

	destino.close();

	ReleaseSRCModel(&src_model);


	

	cout << "{\"result\":1,\"message\":\"success\"}";
	return 1;


}

