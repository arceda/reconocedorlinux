#include "utils.h"
#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <vector>

using namespace std;


//
// TRAIN  V 1.0
// Encargado del entrenamiento.
// Parametros:
// train_sample_list_file   -> Archivo con la lsita de imagenes par el entrenamiento
// n_subject_samples		-> Cantidad de imagenes por persona (30)
// sample_width				-> Ancho a donde se escalara cada imagen, se recomindo 11
// sample_height            -> Ancho a donde se escalara cada imagen, se recomindo 12
// src_model_file           -> Ruta dond se guardara el modelo        
//
//parametros C:\Servidor\Apache\htdocs\videoFirma\faces\trainingList.txt 15 11 12 C:\Servidor\Apache\htdocs\videoFirma\faces\model_xxx.txt

/*
// print usage information
inline void Usage(const char *exe) {
	cerr << "Usage: " << endl
		<< "  " << exe << "train_sample_list_file n_subject_samples sample_width sample_height src_model_file" << endl;
}
*/

int main(int ac, char **av) {
	//cout << "TRAINING..." << endl << endl;

	const string train_sample_list_file = av[1];
	const size_t n_subject_samples = atoi(av[2]);
	const int sample_width = atoi(av[3]);
	const int sample_height = atoi(av[4]);
	const string src_model_file = av[5];

	//const string train_sample_list_file = "train_list.txt";
	//const size_t n_subject_samples = 10;
	//const int sample_width = 11;
	//const int sample_height = 12;
	//const string src_model_file = "my_model.txt";

	

	//const string train_sample_list_file = "D:\\yaleBD\\croppedYaleTraining.txt"; //archivo con la lsita de fotos a entrenar
	//const size_t n_subject_samples = atoi("30"); //nuemero de muestras por sujeto
	//const int sample_width = atoi("10"); //tama�o de la imagen si no concuerda con l afoto se escala: 168, a mas tama�o el modelo crecera mucho pero es mas acertado
	//const int sample_height = atoi("11"); //tama�o de la imagen: 192
	//const string src_model_file = "D:\\yaleBD\\modelCroppedYaleB_2.SRC"; //el modelo se guarda aqui


	CvSize sample_size = cvSize(sample_width, sample_height);


	vector<string> train_sample_list;
	vector<string> train_sample_list_names;

	//cout<<"start LoadSampleListFormat2"<<endl;
	
	LoadSampleListFormat2(train_sample_list_file, &train_sample_list, &train_sample_list_names);

	//cout<<"end LoadSampleListFormat2"<<endl;
	//cout << train_sample_list.size()<<endl;

	assert(train_sample_list.size() % n_subject_samples == 0);

	SRCModel *src = TrainSRCModelFormat2(train_sample_list, train_sample_list_names, sample_size, n_subject_samples);
	SaveSRCModel(src, src_model_file);
	ReleaseSRCModel(&src);

	cout << "{\"result\":1,\"message\":\"success\"}";
	return 1;
}

