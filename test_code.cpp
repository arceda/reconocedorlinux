
#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>
#include<iterator>

//#include "opencv2/core/core.hpp"
//#include "opencv2/contrib/contrib.hpp"
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc/imgproc.hpp"
//#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

CvMat* LoadSamples(
	const vector<string> &sample_list, // sample list, subject by subject
	const CvSize &sample_size // size of sample. if not equal to size of sample, resize to
	) {
	
	/*CvMat *A = cvCreateMat(sample_list.size(), sample_size.width*sample_size.height, CV_64FC1);
	IplImage *resz = cvCreateImage(sample_size, IPL_DEPTH_8U, 1);
	IplImage *resz_db = cvCreateImage(sample_size, IPL_DEPTH_64F, 1);

	for (size_t i = 0; i<sample_list.size(); ++i) {
		IplImage *image = cvLoadImage(sample_list[i].c_str(), CV_LOAD_IMAGE_GRAYSCALE);	

		//cvSaveImage((sample_list[i] + ".jpg").c_str(), image); //las guardo en jpg porsiaca

		cvResize(image, resz, CV_INTER_LINEAR);
		cvConvertScale(resz, resz_db, 1.0);
		cvNormalize(resz_db, resz_db, 1.0, 0, CV_L2);
		memcpy(cvPtr2D(A, i, 0), resz_db->imageData, sizeof(double)*A->cols);
		cvReleaseImage(&image);
	}
	cvReleaseImage(&resz);
	cvReleaseImage(&resz_db);
	return A;*/

	//Mat A = Mat(sample_list.size(), sample_size.width*sample_size.height, CV_64FC1);
	Mat A;
	
	for (size_t i = 0; i<sample_list.size(); ++i) {
		//hacemos un resize y normalizamos
		Mat image = imread(sample_list[i].c_str(), 0);

		resize(image, image, sample_size, CV_INTER_LINEAR);

		//cout<<image;

		Mat tmp;
		
		normalize(image, tmp, 0.0, 1.0, NORM_MINMAX, CV_32FC1);	

		cout<<image;		
		cout<<tmp;	

		//2d matrix to 1d
		Mat image_1d = image.reshape(0,1);
		//agregamos la imagen a A
		A.push_back(image_1d);

		//waitKey(0); 
	}

	cout<<A;

	CvMat result = A;
	return &result;
	
}

int main(){
	cout<<"Hola, empezaremos algunos test.."<<endl;

	string a = "lena.jpg", b = "mona.bmp";
	vector<string> sample_list = vector<string>();
	sample_list.push_back(a);
	sample_list.push_back(b);
	CvSize my_size = cvSize(5,5);

	LoadSamples(sample_list, my_size);

	cout<<"finish"<<endl;

	

	return 0;
}
